use std::{
    fmt,
    fs::{File, OpenOptions},
    io::{self, Seek, Write},
    time::{Duration, Instant},
};

use serde::{Deserialize, Deserializer};
use serde_with::{serde_as, DurationSeconds};

use thiserror::Error;

impl fmt::Display for Scraper {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}/{}] {} - {}",
            self.started.duration_since(self.now).as_secs(),
            self.duration.as_secs(),
            self.artist,
            self.title
        )
    }
}

#[derive(Debug, Error)]
enum ScrapeError {
    #[error("request returned an error: {0}")]
    Request(#[from] Box<ureq::Error>),
    #[error("error on io: {0}")]
    IoError(#[from] io::Error),
}

#[serde_as]
#[derive(Deserialize)]
struct Scraper {
    artist: String,
    title: String,
    #[serde(deserialize_with = "deserialize_instant")]
    now: Instant,
    #[serde(deserialize_with = "deserialize_instant")]
    started: Instant,
    #[serde_as(as = "DurationSeconds<u64>")]
    duration: Duration,
    #[serde_as(as = "DurationSeconds<u64>")]
    nextpoll: Duration,
    #[serde(skip)]
    #[serde(default = "Instant::now")]
    lastpoll: Instant,
}

pub fn deserialize_instant<'de, D>(deserializer: D) -> Result<Instant, D::Error>
where
    D: Deserializer<'de>,
{
    let seconds: u64 = Deserialize::deserialize(deserializer)?;
    let duration = Duration::from_secs(seconds);
    Ok(Instant::now()
        .checked_sub(duration)
        .unwrap_or(Instant::now()))
}

impl Scraper {
    const DEFAULT_DELAY: u64 = 30;
    const SCENESAT_URL: &'static str = "https://scenesat.com/playing";
    const TICK: Duration = Duration::from_secs(3);

    fn new() -> Result<Self, ScrapeError> {
        Self::scrape_current_track()
    }

    fn scrape_current_track() -> Result<Self, ScrapeError> {
        Ok(ureq::get(Self::SCENESAT_URL)
            .call()
            .map_err(Box::new)?
            .into_json::<Self>()
            .map_err(|e| {
                let err = e.into();
                Box::new(err)
            })?)
    }

    fn need_poll(&mut self) -> bool {
        let elapsed = self.lastpoll.elapsed().as_secs();
        let is_poll = elapsed > self.nextpoll.as_secs() || elapsed > Self::DEFAULT_DELAY;
        self.nextpoll -= Self::TICK;

        is_poll
    }

    fn poll(&mut self) -> Result<(), ScrapeError> {
        *self = Self::scrape_current_track()?;
        Ok(())
    }

    fn write_new_content(&self, f: &mut File) -> Result<(), ScrapeError> {
        let new_content = self.to_string();
        f.seek(io::SeekFrom::Start(0))?;
        f.write_all(new_content.as_bytes())?;
        f.set_len(new_content.as_bytes().len() as u64)?;

        Ok(())
    }
}

fn main() {
    let file = std::env::args().nth(1).expect("need file to update");
    let mut scraper = Scraper::new().expect("error scraping scenesat url");
    let mut update_file = OpenOptions::new()
        .create(true)
        .write(true)
        .append(false)
        .truncate(true)
        .open(file)
        .expect("could not update file");

    scraper
        .write_new_content(&mut update_file)
        .expect("error writing content");

    loop {
        if scraper.need_poll() {
            scraper.poll().expect("error scraping scenesat");
            scraper
                .write_new_content(&mut update_file)
                .expect("error writing content");
        }
        std::thread::sleep(Scraper::TICK);
    }
}
