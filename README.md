# Scenesat Text Scraper (Rust Port)

Scenesat Text Scraper is a Rust port of a tool originally written in Python for scraping text data related to Scenesat player.

## Usage

To use the Scenesat Text Scraper, follow these steps:

1. Clone this repository.
2. Build the project using Cargo (Rust's package manager).
3. Run the executable with specified input parameters.

Example:
```shell
$ ./scenesat_text_scraper <output_file>
```

## Contributions

Contributions, issues, and feature requests are welcome! Feel free to submit pull requests or create issues in the GitHub repository.

## License

This project is licensed under the [MIT License](LICENSE.md).
